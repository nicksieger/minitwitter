# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_minitwitter_session',
  :secret      => '45abc2b3543326f77246199f46954459c0fd96742c03f93fa00e841e7ddad86476441f104fcb2c0077ce5b598323f4c7220e9dfd2d98bc71fb8174f9ba776691'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
